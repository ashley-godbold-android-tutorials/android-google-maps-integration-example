In the file [map_key.xml](app/src/main/res/values/map_key.xml), replace `YOUR_API_KEY_GOES_HERE` with an GoogleMaps enabled API key generated at [https://console.developers.google.com/](https://console.developers.google.com/)

When you run the app, you may be required to update google play services on your emulator.